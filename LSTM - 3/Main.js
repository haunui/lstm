const LSTM = require("./LSTM.js");
const Matrix = require("./Matrix");
const dataModule = require("./textfile.js");

var lstm = new LSTM(2,10,2);
lstm.grad_norm = [-1,1];
lstm.learning_rate = 0.001;
lstm.loss_alpha = 0;

var batch_size = 100;
var training_percent = 0.90;
var before_test_sequence_n = 20;

function arraySubstract(a1,a2){
    var result = [];
    for(let i = 0; i < a1.length;i++){
        result.push(a1[i] - a2[i]);
    }

    return result;
}

function msToTime(ms){
    var min = 0;
    var sec = 0;
    var text = "";

    if(ms >= 1000){
        sec = Math.floor(ms/1000);
        ms = ms % 1000;
        
        if(sec > 60){
            min = Math.floor(sec/60);
            sec = sec % 60;

            text = min + " min " + sec + " sec " + ms + " ms";
        }
        else{
            text = sec + " sec " + ms + " ms";
        }
    }else text = ms + " ms";

    return text;
}

// let processed = dataModule.data.split('\n');

// var pre_result = [];
// for(let i = 0; i < processed.length;i++){
//     var cell = processed[i].split(',');
//     cell.shift();

//     for(let j = 0 ; j < cell.length;j++){
//         cell[j] = parseFloat(cell[j]);
//     }
//     if(cell[cell.length - 1] == 0)continue;
//     pre_result.push(cell);
// }

// var max_cell = [0,0,0,0,0];
// var min_cell = JSON.parse(JSON.stringify(pre_result[0]));

// for(let i = 0; i < pre_result.length;i++){
//     for(let j = 0; j < pre_result[i].length ;j++){
//         if(pre_result[i][j] > max_cell[j]) max_cell[j] = pre_result[i][j];
//         else if(pre_result[i][j] < min_cell[j]) min_cell[j] = pre_result[i][j];
//     }
// }

// var result = [];
// for(let i = 0; i < pre_result.length;i++){
//     for(let j = 0; j < pre_result[i].length ;j++){
//         pre_result[i][j] = (pre_result[i][j] - min_cell[j]) / (max_cell[j] - min_cell[j]); 
//     }
//     result.push(pre_result[i]);
// }

var pre_result = [];
for(let i = 0; i < 50000;i++){
    pre_result.push([(Math.sin(i) + 1) / 2]);
}

var result = [];
for(let i = 1; i < pre_result.length ;i++){
    if(pre_result[i] >= pre_result[i-1]){
        result.push([0,1]);
    }else{
        result.push([1,0]);
    }
}


var train_data = [];
var test_data = [];

for(let i = 0; i < result.length; i++){
    if(i < Math.floor(result.length * training_percent)) train_data.push(result[i]);
    else test_data.push(result[i]);
}

var train_count = 0;
var success_count = 0;
var total_train_count = 0;

function train(lstm){

    train_count++;
    var sum = 0;
    var avg_time = 0;

    var test_sample_sum = 0;

    var i = 0, j = train_data.length - 1;
    var iv = setInterval(() => {
        
        var start = Date.now();

        var input = train_data[i];
        var target = train_data[i+1];

        var loss = lstm.backprop(input,target);

        if(i % batch_size == 0) lstm.resetLayers();
        if(isNaN(loss)|| loss < 0){
            console.log("Error, returned : " + loss);
            console.log(input);
            console.log(target);
            console.log("iteration : " + i);
            clearInterval(iv);
        }
        sum += loss;

        var end = Date.now();
        avg_time += (end - start);
        var time_left = Math.floor((avg_time) / (i+1)) * (train_data.length - 1 - i);
        var eta = msToTime(time_left);

        console.log("\n");
        console.log("Iteration no : " + (i+1));
        console.log("Approx time left : " + eta);
        console.log("Average loss : "+  (sum / (i+1)));

        if(++i >= j){
            console.log("Done training");
            test_sample_sum = test(lstm);
            lstm.save("sin_ml");
            clearInterval(iv);
        }

    }, 1);

    return test_sample_sum;
}

const fs = require('fs');


function test(lstm){

    console.log("TESTING : ");

    var txt = "\n\nTESTING :\n\n";

    var total_sum = 0;
    var batch_num = Math.floor(test_data.length / batch_size);

    //Dividing test data in batches 
    for(let b = 0 ; b < batch_num; b++){

        txt += "new batch : \n";

        lstm.resetLayers(true);

        var outputs = [test_data[before_test_sequence_n + b * batch_size]];
        var actual = [test_data[before_test_sequence_n + b * batch_size]];
    
        txt += "prediction : " + outputs[0] + " , " + " Actual : " + actual[0] + "\n";

        for(let i = 0; i < before_test_sequence_n ;i++){
            lstm.feedforward(test_data[i + b * batch_size]);
        }
        
        for(let j = before_test_sequence_n + 1 ; j < batch_size;j++){
            var out = lstm.feedforward(outputs[outputs.length-1]).matrix[0];
            outputs.push(out);
            actual.push(test_data[(b * batch_size) + j]);

            txt += "prediction : " + out + " , " + " Actual : " + test_data[(b * batch_size) + j] + "\n";
        }

        //computes sum of differences of abs(target - prediction)
        // var sum = arraySubstract(actual,outputs).map(Math.abs).reduce((a,b)=>{return a+b});
        var sum = 0;
        for(let s = 0; s < actual.length; s++){
            if(outputs[s][0] > 0.5 && actual[s][0] == 1)sum++;
            else if(outputs[s][1] > 0.5 && actual[s][1] == 1)sum++;
        }

        txt += "Average difference : " + (sum / outputs.length) + "\n\n";

        total_sum += sum / outputs.length;
    }

    txt += "for " + batch_num + " batches\n";
    txt += "overall average sum : " + (total_sum / batch_num);

    console.log("for " + batch_num + " batches");
    console.log("for " + train_count + " training sessions");
    console.log("overall average sum : " + (total_sum / batch_num)) + "\n";

    fs.writeFileSync('outputs.txt',txt);

    // if(success_count < 10){

    //     if((total_sum / batch_num) < 0.05) {
    //         total_train_count += train_count;
    //         train_count = 0;
    //         success_count++;
    //     }
    //     if(success_count == 10){
    //         console.log("after 10 times average training session to reach goal : " + (total_train_count / 10));
    //         return;
    //     }
    //     console.log("training new lstm : \n\n");
    //     var new_lstm = new LSTM(1,10,1);
    //     new_lstm.n_input = lstm.n_input;
    //     new_lstm.n_hidden = lstm.n_hidden;
    //     new_lstm.n_output = lstm.n_output;

    //     new_lstm.learning_rate = lstm.learning_rate;
    //     new_lstm.grad_norm = lstm.grad_norm;
    //     new_lstm.loss_alpha = lstm.loss_alpha;

    //     train(new_lstm);

    // }else{
    //     console.log("after 10 times average training session to reach goal : " + (total_train_count / 10));
    // }
}

function checkNAN(){
    console.log("wf : " + lstm.wf.hasNAN());
    console.log("uf : " + lstm.uf.hasNAN());
    console.log("bf : " + lstm.bf.hasNAN());

    console.log("wi : " + lstm.wi.hasNAN());
    console.log("ui : " + lstm.ui.hasNAN());
    console.log("bi : " + lstm.bi.hasNAN());

    console.log("wa : " + lstm.wa.hasNAN());
    console.log("ua : " + lstm.ua.hasNAN());
    console.log("ba : " + lstm.ba.hasNAN());

    console.log("wo : " + lstm.wo.hasNAN());
    console.log("uo : " + lstm.uo.hasNAN());
    console.log("bo : " + lstm.bo.hasNAN());

    console.log("wz : " + lstm.wz.hasNAN());
    console.log("bz : " + lstm.bz.hasNAN());
}

function load(name_load,new_lstm){
    
    fs.readFile(name_load+".txt","utf-8",(e,data)=>{
        if(e)throw e;
        var parsed = JSON.parse(data);
        
        console.log(parsed[0]);
        
        new_lstm = new LSTM(parsed[0],parsed[1],parsed[2]);
        
        new_lstm.wf = new Matrix(parsed[3]);
        new_lstm.uf = new Matrix(parsed[4]);
        new_lstm.bf = new Matrix(parsed[5]);

        new_lstm.wi = new Matrix(parsed[6]);
        new_lstm.ui = new Matrix(parsed[7]);
        new_lstm.bi = new Matrix(parsed[8]);

        new_lstm.wa = new Matrix(parsed[9]);
        new_lstm.ua = new Matrix(parsed[10]);
        new_lstm.ba = new Matrix(parsed[11]);

        new_lstm.wo = new Matrix(parsed[12]);
        new_lstm.uo = new Matrix(parsed[13]);
        new_lstm.bo = new Matrix(parsed[14]);

        new_lstm.wz = new Matrix(parsed[15]);
        new_lstm.bz = new Matrix(parsed[16]);

        console.log("done loading lstm");
        test(new_lstm);
    });
    
}

train(lstm);
// test();

// var loaded = new LSTM(1,1,1);
// load('sin_ml',loaded);