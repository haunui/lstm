module.exports = class Matrix{
        constructor(array){
            this.matrix = array;
            this.rows = array.length;
            this.cols = array[0].length;
        }
    
        static add(mat){ 
    
            var result = Matrix.emptyMatrix(mat[0].rows,mat[0].cols);
    
            for(let i = 0; i < mat.length;i++){
    
                //Adding number
                if(typeof mat[i] == "number"){
                    for(let j = 0; j < result.rows; j++){
                        for(let k = 0; k < result.cols; k++){
                            result.matrix[j][k] = result.matrix[j][k] + mat[i];
                        }
                    }
    
                    break;
                }
    
                //Adding matrices
                if(mat[i].cols != result.cols || mat[i].rows != result.rows){
                    console.log(mat);
                    console.log("Can't add matrices : different rows / cols length at i : " + i);
                    console.trace();
                    return;
                }
    
                for(let j = 0; j < mat[i].rows; j++){
                    for(let k = 0; k < mat[i].cols; k++){
                        result.matrix[j][k] = result.matrix[j][k] + mat[i].matrix[j][k];
                    }
                }
            }
    
            return result;
        
    
        }
    
        static substract(m1,m2){
    
            if(typeof m2 == "number") return this.add([m1,-m2]);
    
            return this.add([m1,this.multiply(m2,-1)]);
        }
    
        static multiply(m1,m2){
            
            if(typeof m2 == "number"){
    
                var result = [];
                for(let i = 0; i < m1.rows;i++){
                    var row_result = [];
                    for(let j = 0 ; j < m1.cols;j++){
                        row_result.push(m1.matrix[i][j] * m2);
                    }
                    result.push(row_result);
                }
    
                return new Matrix(result);
            }
    
            if(m1.cols != m2.rows) return;
            
            var result = [];
    
            for(let i = 0; i < m1.rows;i++){
                var row_result = [];
                for(let j = 0; j < m2.cols;j++){
                    var sum = 0;
                    for(let k = 0; k < m1.cols;k++){
                        sum += m1.matrix[i][k] * m2.matrix[k][j];
                    }
                    row_result.push(sum);
                }
    
                result.push(row_result);
            }
    
            return new Matrix(result);
        }
    
        static toArray(mat){
            return mat.matrix[0];
        }
    
        static fromArray(array){
            return new Matrix([array]);
        }
    
        static map(matrix,func){
    
            var result = [];
            for(let i = 0; i < matrix.rows;i++){
                var row_result = [];
                for(let j = 0; j < matrix.cols;j++){
                    row_result.push(func(matrix.matrix[i][j]));
                }
                result.push(row_result);
            }
    
            return new Matrix(result);
        }
    
        map(func){
            
            for(let i = 0; i < this.rows;i++){
                for(let j = 0; j < this.cols;j++){
                    this.matrix[i][j] = func(this.matrix[i][j]);
                }
            }
        }
    
        static transpose(matrix){
            
            var result = [];
        
            for(let i = 0; i < matrix.cols;i++){
                var row_result = [];
                for(let j = 0; j < matrix.rows;j++){
                    row_result.push(matrix.matrix[j][i]);
                }
    
                result.push(row_result);
            }
    
            return new Matrix(result);
        }
    
        static hadamard(mat){
            
            var result = Matrix.add([Matrix.emptyMatrix(mat[0].rows,mat[0].cols),1]);
    
            for(let i = 0; i < mat.length ;i++){
    
                if(mat[i].cols != result.cols || mat[i].rows != result.rows){
                    console.log(mat);
                    console.log("Can't hadamard matrices : different rows / cols length at i : " + i);
                    console.trace();
                    return;
                }
    
                for(let j = 0; j < mat[i].rows ; j++){
                    for(let k = 0; k < mat[i].cols ; k++){
                        result.matrix[j][k] = result.matrix[j][k] * mat[i].matrix[j][k];
                    }
                }
            }
    
            return result;
        }
    
        static emptyMatrix(n,m){
            
            var result = [];
            
            for(let i = 0; i < n;i++){
                var row_result = [];
                for(let j = 0; j < m;j++){
                    row_result.push(0);
                }
    
                result.push(row_result);
            }
    
            return new Matrix(result);
        }
    
        static power(m1,n){
            
            if(n == 0 ) return this.identity(m1.rows);
    
            if(m1.rows != m1.cols){
                console.log("Can't do power of, matrix not square");
                return;
            }
    
            var result = m1;
            
            for(let i = 1; i < n;i++){
                var temp = Matrix.multiply(result,m1);
                result = temp;
            }
    
            return result;
        }
    
        static identity(n){
            var result = [];
            for(let i = 0;i < n;i++){
                var row_result = [];
                for(let j = 0; j < n;j++){
                    row_result.push(i==j?1:0);
                }
                result.push(row_result);
            }
    
            return new Matrix(result);
        }
    
        sum(){
            if(this.rows != 1){
                console.log("Not a 1D matrix");
                return;
            }
    
            var sum = 0;
            for(let i = 0; i < this.cols;i++){
                sum += this.matrix[0][i];
            }
    
            return sum;
        }
        
        clip(bounds){
            for(let i = 0 ; i < this.rows;i++){
                for(let j = 0 ; j < this.cols;j++){
                    if(this.matrix[i][j] > bounds[1])this.matrix[i][j] = bounds[1];
                    else if(this.matrix[i][j] < bounds[0]) this.matrix[i][j] = bounds[0];
                }
            }
        }
    
        hasNAN(){
            for(let i = 0 ; i < this.rows;i++){
                for(let j = 0 ; j < this.cols;j++){
                    if(isNaN(this.matrix[i][j])) return true;
                }
            }
    
            return false;
        }
    
        print(){
            console.log(this.rows + " x " + this.cols + " Matrix");
            console.table(this.matrix);
        }
    
        static divide(m1,m2){
            if(m1.cols != m2.cols || m1.rows != m2.rows){
                console.log("cant divide");
                return;
            }
            var result = [];
            for(let i = 0; i < m1.rows;i++){
                var row_result = [];
                for(let j = 0; j < m1.cols;j++){
    
                    row_result.push(m1.matrix[i][j] / m2.matrix[i][j]);
                }
    
                result.push(row_result);
            }
    
            return new Matrix(result);
        }
    
        toArrays(){
            var result = [];
    
            for(let i = 0 ; i < this.rows;i++){
                var row_result = [];
                for(let j = 0 ; j < this.cols;j++){
                    row_result.push(this.matrix[i][j]);
                }
                result.push(row_result);
            }
    
            return result;
        }
    }