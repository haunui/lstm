const Matrix = require('./Matrix.js');
const fs = require('fs');

module.exports = class LSTM{
    constructor(n_input,n_hidden,n_output){
        this.n_input = n_input;
        this.n_hidden = n_hidden;
        this.n_output = n_output;

        this.layers = [{
            xt:Matrix.emptyMatrix(1,n_input),
            ft:Matrix.emptyMatrix(1,n_hidden),
            it:Matrix.emptyMatrix(1,n_hidden),
            at:Matrix.emptyMatrix(1,n_hidden),
            ot:Matrix.emptyMatrix(1,n_hidden),
            ht:Matrix.emptyMatrix(1,n_hidden),
            ct:Matrix.emptyMatrix(1,n_hidden),
            y:Matrix.emptyMatrix(1,n_output),
            target:Matrix.emptyMatrix(1,n_output),
            loss_ht:undefined,
            cached_loss:undefined,
            d_L_ct : Matrix.emptyMatrix(1,n_hidden)
        }];

        this.wf = this.createWeights(n_hidden,n_hidden);
        this.uf = this.createWeights(n_input,n_hidden);
        this.bf = this.createBias(n_hidden);
        
        this.wi = this.createWeights(n_hidden,n_hidden);
        this.ui = this.createWeights(n_input,n_hidden);
        this.bi = this.createBias(n_hidden);
        
        this.wa = this.createWeights(n_hidden,n_hidden);
        this.ua = this.createWeights(n_input,n_hidden);
        this.ba = this.createBias(n_hidden);
        
        this.wo = this.createWeights(n_hidden,n_hidden);
        this.uo = this.createWeights(n_input,n_hidden);
        this.bo = this.createBias(n_hidden);
        
        this.wz = this.createWeights(n_hidden,n_output);
        this.bz = this.createBias(n_output);

        this.grad_norm = [-1,1];
        this.learning_rate = 0.0001;

        this.loss_alpha = 5;
        this.epsilon = Math.exp(-25);
    }
    
    save(save_name){
        var text = "[";
            text += JSON.stringify(this.n_input) + ",";
            text += JSON.stringify(this.n_hidden) + ",";
            text += JSON.stringify(this.n_output) + ",";

            text += JSON.stringify(this.wf.toArrays()) + ",";
            text += JSON.stringify(this.uf.toArrays()) + ",";
            text += JSON.stringify(this.bf.toArrays()) + ",";

            text += JSON.stringify(this.wi.toArrays()) + ",";
            text += JSON.stringify(this.ui.toArrays()) + ",";
            text += JSON.stringify(this.bi.toArrays()) + ",";

            text += JSON.stringify(this.wa.toArrays()) + ",";
            text += JSON.stringify(this.ua.toArrays()) + ",";
            text += JSON.stringify(this.ba.toArrays()) + ",";

            text += JSON.stringify(this.wo.toArrays()) + ",";
            text += JSON.stringify(this.uo.toArrays()) + ",";
            text += JSON.stringify(this.bo.toArrays()) + ",";

            text += JSON.stringify(this.wz.toArrays()) + ",";
            text += JSON.stringify(this.bz.toArrays());

        text += "]";

        fs.writeFile(save_name+".txt",text,(e)=>{if(e){throw e}});
    }

    createWeights(n_input,n_output){
        
        var result = [];
        for(let i = 0; i < n_input;i++){
            var row_result = [];
            for(let j = 0; j < n_output;j++){
                //Xavier initialization
                row_result.push((((Math.random() * 2) - 1) * Math.sqrt(6/(n_input + n_output))));
            }

            result.push(row_result);
        }

        return new Matrix(result);
    }

    createBias(num){
        var result = [];
        for(let i = 0; i < num;i++){
            result.push(0);
        }

        return new Matrix([result]);
    }

    sigmoid(x){
        return (1 / (1 + Math.exp(-x)));
    }

    dsigmoid(y){
        return y * (1 - y);
    }

    tanh(x){
        return ((Math.exp(x) - Math.exp(-x))/(Math.exp(x) + Math.exp(-x)));
    }

    dtanh(y){
        return 1 - Math.pow(y,2);
    }

    //input : matrix
    softmax(x){

        var max = 0;
        for(let i = 0; i < x.rows;i++){
            for(let j = 0; j < x.cols;j++){
                if(x.matrix[i][j] > max) max = x.matrix[i][j];
            }
        }

        var x2 = Matrix.map(x,(e)=>{return e - max});
        var result = [];
        var sum = 0;

        for(let i = 0; i < x2.rows;i++){
            var row_result = [];
            for(let j = 0; j < x2.cols;j++){
                let exp = Math.exp(x2.matrix[i][j]);
                row_result.push(exp);
                sum += exp;
            }

            result.push(row_result);
        }

        return Matrix.map(new Matrix(result),(e) => {return e/sum});
    }

    dsoftmax(y){
        return y * (1-y);
    }

    reLU(x){
        return x > 0 ? x : 0;
    }

    dreLU(y){
        return y > 0 ? 1 : 0;
    }

    feedforward(input){

        var previous_ht = this.layers[this.layers.length - 1].ht;
        var previous_ct = this.layers[this.layers.length - 1].ct;
        var xt = Matrix.fromArray(input);

        var zft = Matrix.add([
            Matrix.multiply(previous_ht,this.wf),
            Matrix.multiply(xt,this.uf) , this.bf
        ]);
        
        var ft = Matrix.map(zft,this.sigmoid);

        var zit = Matrix.add([
            Matrix.multiply(previous_ht,this.wi), 
            Matrix.multiply(xt,this.ui) , this.bi
        ]);
        
        var it = Matrix.map(zit,this.sigmoid);

        var zat = Matrix.add([
            Matrix.multiply(previous_ht,this.wa), 
            Matrix.multiply(xt,this.ua) , this.ba
        ]);
        
        var at = Matrix.map(zat,this.tanh);

        var zot = Matrix.add([
            Matrix.multiply(previous_ht,this.wo), 
            Matrix.multiply(xt,this.uo) , this.bo
        ]);
        
        var ot = Matrix.map(zot,this.sigmoid);

        var ct = Matrix.add([
            Matrix.hadamard([previous_ct,ft]), 
            Matrix.hadamard([it,at])
        ]);

        var ht = Matrix.hadamard([Matrix.map(ct,this.tanh),ot]);

        var zt = Matrix.add([Matrix.multiply(ht,this.wz) , this.bz]);
        var y = this.softmax(zt);
        // var y = Matrix.map(zt,this.sigmoid);
        // var y = Matrix.map(zt,this.reLU);

        this.layers.push({xt:xt,ft:ft,it:it,at:at,ot:ot,ht:ht,ct:ct,y:y});
        return y;
    }   

    backprop(input,target){

        //Add the last layer
        this.feedforward(input);

        var last_cached_loss;
        //If we use the log-scaled Loss function
        if(this.loss_alpha > 0){
            last_cached_loss = Matrix.hadamard([
                Matrix.add([
                    Matrix.hadamard([
                        this.layers[this.layers.length -1].y,
                        Matrix.substract(
                            this.layers[this.layers.length -1].y,
                            Matrix.fromArray(target)
                        )
                    ]),
                    Matrix.multiply(
                        Matrix.substract(
                            Matrix.map(this.layers[this.layers.length -1].y,(e) => {return Math.log(e + this.epsilon)}),
                            Matrix.map(Matrix.fromArray(target),(e) => {return Math.log(e + this.epsilon)})
                        ),
                        this.loss_alpha
                    )
                ]),
                Matrix.map(this.layers[this.layers.length -1].y,(e) => {return 1 - e})
            ]);
        }else{
            last_cached_loss = Matrix.hadamard([
                Matrix.substract(
                    this.layers[this.layers.length - 1].y,
                    Matrix.fromArray(target)
                ),
                Matrix.map(this.layers[this.layers.length - 1].y,this.dsigmoid)
            ]);
        }
        

        this.layers[this.layers.length - 1].cached_loss = last_cached_loss;
        this.layers[this.layers.length - 1].target = Matrix.fromArray(target);

        //Where backprop begins
        for(let i = this.layers.length - 1 ; i >= 1 ; i--){

            var xt = this.layers[i].xt;
            var ft = this.layers[i].ft;
            var it = this.layers[i].it;
            var at = this.layers[i].at;
            var ot = this.layers[i].ot;
            var ct = this.layers[i].ct;
            var ht = this.layers[i].ht;

            //Computing loss here (to optimize / cache later too tired now)
            var loss_ht = Matrix.multiply(
                this.layers[i].cached_loss,
                Matrix.transpose(this.wz)
            );

            if(i != this.layers.length - 1){
                loss_ht = Matrix.add([loss_ht,this.layers[i+1].loss_ht]);
            }

            //dL / dct
            var d_L_ct = Matrix.hadamard([
                loss_ht,
                ot,
                Matrix.map(ct,(e)=>{return 1 - Math.pow(this.tanh(e),2)})
            ]);

            //If not at last step, we can compound error from next step
            if(i != this.layers.length - 1){
                d_L_ct = Matrix.add([
                    d_L_ct,
                    Matrix.hadamard([
                        this.layers[i+1].d_L_ct,
                        this.layers[i+1].ft
                    ])
                ]);
            }

            this.layers[i].d_L_ct = d_L_ct;

            //Gradient Forget Gate
            var g_ft = Matrix.hadamard([
                d_L_ct,
                this.layers[i-1].ct,
                Matrix.map(ft,this.dsigmoid)
            ]);

            //Gradient Input Gate 1
            var g_it = Matrix.hadamard([
                d_L_ct,
                at,
                Matrix.map(it,this.dsigmoid)
            ]);

            //Gradient Input Gate 2
            var g_at = Matrix.hadamard([
                d_L_ct,
                it,
                Matrix.map(at,this.dtanh)
            ]);

            //Gradient Output Gate 
            var g_ot = Matrix.hadamard([
                loss_ht,
                Matrix.map(ct,this.tanh),
                Matrix.map(ot,this.dsigmoid)
            ]);

            //Backpropagates the loss for each gate
            //Add it up times W to get backpropagated loss at ht-1
            var d_l_ht = Matrix.add([
                Matrix.multiply(
                    g_ft,
                    Matrix.transpose(this.wf)
                ),
                Matrix.multiply(
                    g_it,
                    Matrix.transpose(this.wi)
                ),
                Matrix.multiply(
                    g_at,
                    Matrix.transpose(this.wa)
                ),
                Matrix.multiply(
                    g_ot,
                    Matrix.transpose(this.wo)
                )
            ]);

            this.layers[i].loss_ht = d_l_ht;

            //Weights and Biases

            //Forget w/u/b
            var g_wf = Matrix.multiply(
                Matrix.transpose(this.layers[i-1].ht),
                g_ft
            );

            var g_uf = Matrix.multiply(
                Matrix.transpose(xt),
                g_ft
            );

            var g_bf = g_ft;

            //Input1 w/u/b
            var g_wi = Matrix.multiply(
                Matrix.transpose(this.layers[i-1].ht),
                g_it
            );

            var g_ui = Matrix.multiply(
                Matrix.transpose(xt),
                g_it
            );

            var g_bi = g_it;

            //Input2 w/u/b
            var g_wa = Matrix.multiply(
                Matrix.transpose(this.layers[i-1].ht),
                g_at
            );

            var g_ua = Matrix.multiply(
                Matrix.transpose(xt),
                g_at
            );

            var g_ba = g_at;
            

            //Output w/u/b
            var g_wo = Matrix.multiply(
                Matrix.transpose(this.layers[i-1].ht),
                g_ot
            );

            var g_uo = Matrix.multiply(
                Matrix.transpose(xt),
                g_ot
            );

            var g_bo = g_ot;

            //Final output 
            var g_final = this.layers[i].cached_loss;
            // var g_final = this.layers[i].loss_ht;


            var g_wz = Matrix.multiply(
                Matrix.transpose(ht),
                g_final    
            );

            var g_bz = g_final;


            //Clippin the gradzzz
            g_wf.clip(this.grad_norm);
            g_uf.clip(this.grad_norm);
            g_bf.clip(this.grad_norm);

            g_wi.clip(this.grad_norm);
            g_ui.clip(this.grad_norm);
            g_bi.clip(this.grad_norm);

            g_wa.clip(this.grad_norm);
            g_ua.clip(this.grad_norm);
            g_ba.clip(this.grad_norm);

            g_wo.clip(this.grad_norm);
            g_uo.clip(this.grad_norm);
            g_bo.clip(this.grad_norm);

            g_wz.clip(this.grad_norm);
            g_bz.clip(this.grad_norm);

            if(g_wi.hasNAN() || g_ui.hasNAN() || g_bi.hasNAN()) return -2;
            else if(g_wf.hasNAN() || g_uf.hasNAN() || g_bf.hasNAN()) return -1;
            else if(g_wa.hasNAN() || g_ua.hasNAN() || g_ba.hasNAN()) return -3;
            else if(g_wo.hasNAN() || g_uo.hasNAN() || g_bo.hasNAN()) return -4;
            else if(g_wz.hasNAN()  || g_bz.hasNAN()) return -5;
    

            //Updatin the weights & biases
            this.wf = Matrix.substract(this.wf,Matrix.multiply(g_wf,this.learning_rate));
            this.uf = Matrix.substract(this.uf,Matrix.multiply(g_uf,this.learning_rate));
            this.bf = Matrix.substract(this.bf,Matrix.multiply(g_bf,this.learning_rate));

            this.wi = Matrix.substract(this.wi,Matrix.multiply(g_wi,this.learning_rate));
            this.ui = Matrix.substract(this.ui,Matrix.multiply(g_ui,this.learning_rate));
            this.bi = Matrix.substract(this.bi,Matrix.multiply(g_bi,this.learning_rate));

            this.wa = Matrix.substract(this.wa,Matrix.multiply(g_wa,this.learning_rate));
            this.ua = Matrix.substract(this.ua,Matrix.multiply(g_ua,this.learning_rate));
            this.ba = Matrix.substract(this.ba,Matrix.multiply(g_ba,this.learning_rate));

            this.wo = Matrix.substract(this.wo,Matrix.multiply(g_wo,this.learning_rate));
            this.uo = Matrix.substract(this.uo,Matrix.multiply(g_uo,this.learning_rate));
            this.bo = Matrix.substract(this.bo,Matrix.multiply(g_bo,this.learning_rate));

            this.wz = Matrix.substract(this.wz,Matrix.multiply(g_wz,this.learning_rate));
            this.bz = Matrix.substract(this.bz,Matrix.multiply(g_bz,this.learning_rate));
        }
        
        var loss = Matrix.add([
            Matrix.map(
                Matrix.substract(
                    this.layers[this.layers.length-1].y,
                    Matrix.fromArray(target)),
                (e)=>{return Math.pow(e,2)}),
            Matrix.map(
                Matrix.multiply(
                    Matrix.substract(
                        Matrix.map(this.layers[this.layers.length-1].y,(e) => {return Math.log(e + this.epsilon)}),
                        Matrix.map(Matrix.fromArray(target),(e) => {return Math.log(e + this.epsilon)})
                    ),
                    this.loss_alpha
                ),
                (e)=>{return Math.pow(e,2)}
            )
        ]);

        return loss.sum();
    }

    resetLayers(empty){
        var last = this.layers[this.layers.length - 1];
        this.layers = [last];
        
        if(empty){
            this.layers = [{
                xt:Matrix.emptyMatrix(1,this.n_input),
                ft:Matrix.emptyMatrix(1,this.n_hidden),
                it:Matrix.emptyMatrix(1,this.n_hidden),
                at:Matrix.emptyMatrix(1,this.n_hidden),
                ot:Matrix.emptyMatrix(1,this.n_hidden),
                ht:Matrix.emptyMatrix(1,this.n_hidden),
                ct:Matrix.emptyMatrix(1,this.n_hidden),
                y:Matrix.emptyMatrix(1,this.n_output),
                target:Matrix.emptyMatrix(1,this.n_output),
                loss_ht:undefined,
                cached_loss:undefined,
                d_L_ct : Matrix.emptyMatrix(1,this.n_hidden)
            }];
        }
    }
}